package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.User;
import uz.pdp.warehouse.projection.UserProjection;

@RepositoryRestResource(path = "user",collectionResourceRel = "listUser",excerptProjection = UserProjection.class)
public interface UserRepository extends JpaRepository<User,Integer> {
}
