package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Attechment;
import uz.pdp.warehouse.entity.AttechmentContent;

import javax.persistence.OneToOne;

@Projection(types = AttechmentContent.class)
public interface CustomAttechmentContent {

    Integer getId();

    byte[] getBytes();

    Attechment getAttechment();
}
