package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.AttechmentContent;
import uz.pdp.warehouse.projection.CustomAttechmentContent;

@RepositoryRestResource(path = "attechmentContent",collectionResourceRel = "list",excerptProjection = CustomAttechmentContent.class)
public interface AttechmentContentRepository extends JpaRepository<AttechmentContent,Integer> {


}
