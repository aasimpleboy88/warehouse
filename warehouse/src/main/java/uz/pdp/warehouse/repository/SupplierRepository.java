package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.Supplier;
import uz.pdp.warehouse.projection.SupplierProjection;

@RepositoryRestResource(path = "supplier",collectionResourceRel = "listSupplier",excerptProjection = SupplierProjection.class)
public interface SupplierRepository extends JpaRepository<Supplier,Integer> {

}
