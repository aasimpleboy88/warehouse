package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.Attechment;
import uz.pdp.warehouse.projection.CustomAttechment;

@RepositoryRestResource(path = "attechment",collectionResourceRel = "list",excerptProjection = CustomAttechment.class)
public interface AttechmentRepository extends JpaRepository<Attechment,Integer> {


}
