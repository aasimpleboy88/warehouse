package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.Input;
import uz.pdp.warehouse.projection.InputProjection;

@RepositoryRestResource(path = "input",collectionResourceRel = "listInput",excerptProjection = InputProjection.class)
public interface InputRepository extends JpaRepository<Input,Integer> {


}
