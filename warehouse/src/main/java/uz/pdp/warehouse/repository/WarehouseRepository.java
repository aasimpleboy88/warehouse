package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.Warehouse;
import uz.pdp.warehouse.projection.WarehouseProjection;

@RepositoryRestResource(path = "warehouse",collectionResourceRel = "listWarehouse",excerptProjection = WarehouseProjection.class)
public interface WarehouseRepository extends JpaRepository<Warehouse,Integer> {
}
