package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.Output;
import uz.pdp.warehouse.projection.OutputProjection;

@RepositoryRestResource(path = "output",collectionResourceRel = "listOutput",excerptProjection = OutputProjection.class)
public interface OutputRepository extends JpaRepository<Output,Integer> {

}
