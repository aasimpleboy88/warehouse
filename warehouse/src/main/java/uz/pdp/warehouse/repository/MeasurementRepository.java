package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.Measurement;
import uz.pdp.warehouse.projection.MeasurementProjection;

@RepositoryRestResource(path = "measurement",collectionResourceRel = "listMeasurement",excerptProjection = MeasurementProjection.class)
public interface MeasurementRepository extends JpaRepository<Measurement,Integer> {
}
