package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Client;
import uz.pdp.warehouse.entity.Currency;
import uz.pdp.warehouse.entity.Output;
import uz.pdp.warehouse.entity.Warehouse;

import java.sql.Date;

@Projection(types = Output.class)
public interface OutputProjection {

    Integer getId();
    Date getDate();
    Warehouse getWarehouse();
    Currency getCurrency();
    String getFacture_number();
    String getCode();
    Client getClient();


}

