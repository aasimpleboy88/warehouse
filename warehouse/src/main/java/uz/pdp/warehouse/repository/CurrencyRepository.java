package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.Currency;
import uz.pdp.warehouse.projection.CurrencyProjection;

@RepositoryRestResource(path = "currency",collectionResourceRel = "listCurrency",excerptProjection = CurrencyProjection.class)
public interface CurrencyRepository extends JpaRepository<Currency,Integer> {

}
