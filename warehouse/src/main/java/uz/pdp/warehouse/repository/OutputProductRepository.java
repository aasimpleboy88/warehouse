package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.OutputProduct;
import uz.pdp.warehouse.projection.OutputProductProjection;

@RepositoryRestResource(path = "outputProduct",collectionResourceRel = "listOutputProduct",excerptProjection = OutputProductProjection.class)
public interface OutputProductRepository extends JpaRepository<OutputProduct,Integer> {


}
