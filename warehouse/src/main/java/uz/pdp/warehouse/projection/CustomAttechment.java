package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Attechment;

@Projection(types = Attechment.class)
public interface CustomAttechment {

    Integer getId();

    String getName();

    long getSize();

    String getContent_type();



}
