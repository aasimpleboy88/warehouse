package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.InputProduct;
import uz.pdp.warehouse.projection.InputProductProjection;

@RepositoryRestResource(path = "inputProduct",collectionResourceRel = "listInputProduct",excerptProjection = InputProductProjection.class)
public interface InputProductRepository extends JpaRepository<InputProduct,Integer> {

}
