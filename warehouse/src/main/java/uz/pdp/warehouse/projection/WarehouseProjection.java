package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Warehouse;

@Projection(name = "warehouseProjection", types = Warehouse.class)
public interface WarehouseProjection {


    Integer getId();
    String getName();
    boolean isActive();

}
