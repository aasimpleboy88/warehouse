package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Warehouse;

import java.util.Set;

@Projection(types = UserProjection.class)
public interface UserProjection {

    Integer getId();
    String getFirst_name();
    String getLast_name();
    String getPhone_number();
    String getCode();
    Set<Warehouse> getWarehouses();
    boolean isActive();

}
