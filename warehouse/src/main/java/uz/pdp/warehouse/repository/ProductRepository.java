package uz.pdp.warehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.warehouse.entity.Product;
import uz.pdp.warehouse.projection.ProductProjection;

@RepositoryRestResource(path = "product",collectionResourceRel = "listProduct",excerptProjection = ProductProjection.class)
public interface ProductRepository extends JpaRepository<Product,Integer> {


}
