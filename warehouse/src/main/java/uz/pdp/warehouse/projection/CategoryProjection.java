package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Category;

@Projection(types = Category.class)
public interface CategoryProjection {

    Integer getId();

    String getName();

    boolean isActive();

    Category getParent_category_id();
}
