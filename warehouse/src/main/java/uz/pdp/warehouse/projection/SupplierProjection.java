package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Supplier;

@Projection(types = Supplier.class)
public interface SupplierProjection {
    Integer getId();
    String getName();
    boolean isActive();
    String getPhone_number();


}
