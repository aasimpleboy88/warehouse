package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Input;
import uz.pdp.warehouse.entity.InputProduct;
import uz.pdp.warehouse.entity.Product;

import java.sql.Date;

@Projection(types = InputProduct.class)
public interface InputProductProjection {

    Integer getId();
    Product getProduct();
    double getAmount();
    double getPrice();
    Date getExpire_date();
    Input getInput();

}

