package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Attechment;
import uz.pdp.warehouse.entity.Category;
import uz.pdp.warehouse.entity.Measurement;
import uz.pdp.warehouse.entity.Product;

@Projection(types = Product.class)
public interface ProductProjection {
    Integer getId();
    String getName();
    boolean isActive();
    Category getCategory();
    Attechment getPhoto();
    String getCode();
    Measurement getMeasurement();


}
