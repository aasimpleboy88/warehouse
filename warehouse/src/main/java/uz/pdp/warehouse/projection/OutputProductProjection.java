package uz.pdp.warehouse.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.warehouse.entity.Output;
import uz.pdp.warehouse.entity.OutputProduct;
import uz.pdp.warehouse.entity.Product;

@Projection(types = OutputProduct.class)
public interface OutputProductProjection {

    Integer getId();
    Product getProduct();
    double getAmount();
    double getPrice();
    Output getOutput();


}
